sudo ip link set dev vodredis@tesis up

sudo qemu-system-x86_64 \
-append 'netdev.ip=172.20.0.12/16:172.20.0.1:::: env.vars=[ "REDIS_IP=172.20.0.12" ] -- /usr/bin/redis-server /etc/redis/redis.conf' \
-cpu host,+x2apic,-pmu \
-device virtio-net-pci,mac=02:b0:b0:73:23:01,netdev=hostnet0 \
-device pvpanic \
-device isa-debug-exit \
-enable-kvm \
-kernel /home/jhon/tesis/vodredis/.unikraft/build/redis_qemu-x86_64 \
-machine pc,accel=kvm \
-m size=512M \
-name 288b1fef-9960-4df7-8ea5-b791244bf136 \
-netdev tap,id=hostnet0,ifname=vodworker@tesis,script=no,downscript=no \
-no-reboot \
-S \
-s \
-parallel none \
-rtc base=utc \
-smp cpus=1,threads=1,sockets=1 \
-vga none \
-serial stdio

